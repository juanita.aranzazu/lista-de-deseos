import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, IonList } from '@ionic/angular';
import { Lista } from 'src/app/models/lista-model';
import { DeseosService } from 'src/app/services/deseos.service';

@Component({
  selector: 'app-listas',
  templateUrl: './listas.component.html',
  styleUrls: ['./listas.component.scss'],
})
export class ListasComponent implements OnInit {
  @ViewChild(IonList) lista: IonList

  @Input() terminada: boolean = true;

  constructor(
    public deseosService: DeseosService,
    private roter: Router,
    private allertCtrl: AlertController) { }

  ngOnInit() {}

  listSelected(list: Lista){
    if(this.terminada){
      this.roter.navigateByUrl(`/tabs/tab2/agregar/${list.id}`);
    } else{
      this.roter.navigateByUrl(`/tabs/tab1/agregar/${list.id}`);
    }
  }

  deleteList(lista: Lista){
    this.deseosService.deleteList(lista);
  }

  async editList(list: Lista){
    const alert = await this.allertCtrl.create({
      cssClass: 'my-custom-class',
      inputs: [{
        name: 'titulo',
        type: 'text',
        value: list.title,
        placeholder: 'nombre de la lista',
      }],
      buttons: [{
        text: 'Cancelar',
        role: 'cancel',
        handler: ()=>{
          console.log('cancelar');
          this.lista.closeSlidingItems();
        }
       },
        {
          text: 'Actualizar',
          handler: (data)=>{
            if(data.titulo.length ===0){
              return;
            };

            list.title= data.titulo;
            this.deseosService.saveStorage();
            this.lista.closeSlidingItems();
          }
        }
      ]
    });
   alert.present();
  }

}

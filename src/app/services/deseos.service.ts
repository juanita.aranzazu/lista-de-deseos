import { Injectable } from '@angular/core';
import { Lista } from '../models/lista-model';

@Injectable({
  providedIn: 'root'
})
export class DeseosService {

  listas:Lista[]= [];

  constructor() {
    this.loadStorage();
   }

   createlist(titulo:string){
    const nuevaLista = new Lista(titulo);
    this.listas.push(nuevaLista);
    this.saveStorage();
    return nuevaLista.id;
   }

   deleteList(lista:Lista){
     this.listas=this.listas.filter((listaDta)=>listaDta.id !== lista.id);
     this.saveStorage();
   }

   saveStorage(){
    localStorage.setItem('data',JSON.stringify(this.listas));
   }

   loadStorage(){
     if(localStorage.getItem('data')){
       this.listas =JSON.parse(localStorage.getItem('data'));
     }
   }

   getList(id:string | number){
    id= Number(id);

    return this.listas.find((listaData)=>listaData.id ===id)
   }
}

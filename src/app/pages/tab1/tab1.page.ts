import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { DeseosService } from 'src/app/services/deseos.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  constructor(
    public deseosService: DeseosService,
    private roter: Router,
    private AlertController: AlertController
    ) {}

    /*El async transforma cualquier metodo en una promesa*/
   async addList(){
      //this.roter.navigateByUrl('/tabs/tab1/agregar');
      /*el await de la creacion de esta promesa es para que se espere hasta que se ejecute
      la funcion y la guarde en la constante*/
      const alert = await this.AlertController.create({
        cssClass: 'my-custom-class',
        inputs: [{
          name: 'titulo',
          type: 'text',
          placeholder: 'nombre de la lista',
        }],
        buttons: [{
          text: 'Cancelar',
          role: 'cancel',
          handler: ()=>{
            console.log('cancelar');
          }
         },
          {
            text: 'Crear',
            handler: (data)=>{
              if(data.titulo.length ===0){
                return;
              };
              const listaId=this.deseosService.createlist(data.titulo);
              this.roter.navigateByUrl(`/tabs/tab1/agregar/${listaId}`);
            }
          }
        ]
      });
     alert.present();
    }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ListaItem } from 'src/app/models/lista-item-model';
import { Lista } from 'src/app/models/lista-model';
import { DeseosService } from 'src/app/services/deseos.service';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.page.html',
  styleUrls: ['./agregar.page.scss'],
})
export class AgregarPage implements OnInit {

  lista: Lista;
  nameItem:string;

  constructor(
    private deseosService: DeseosService,
    private router:ActivatedRoute
    ) {
      const listaId = this.router.snapshot.paramMap.get('listaId')
      this.lista = this.deseosService.getList(listaId);
  }

  ngOnInit() {
  }

  addItem(){
    if(this.nameItem.length === 0){
      return;
    }
    const newItem = new ListaItem(this.nameItem );
    this.lista.items.push(newItem);
    this.nameItem = '';
    this.deseosService.saveStorage();
  }

  changeCheck(item){
    const pendientes = this.lista.items.filter((itemData=>!itemData.completado)).length;
    if(pendientes === 0){
      this.lista.finishIn = new Date();
      this.lista.completed = true;
    }else{
      this.lista.finishIn = null;
      this.lista.completed = false;
    }
   this.deseosService.saveStorage();
  }

  delete(i:number){
    this.lista.items.splice(i,1);
    this.deseosService.saveStorage();
  }

}
